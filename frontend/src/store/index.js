import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    colorAuthor: true,
    colorTitle: true,
    colorContent: true,

    showTable: false,
    tableTitles: [],
    usedSwitches: {},
    term: "",
    activeTableIndex: "",

    images: [],
    pageOptions: {},
    totalCount: 0,
    loading: false,
  },
  getters: {
    getTableImages: (state) => {
      return state.images.filter((obj) => obj.detection_type == "tables");
    },
    getImageImages: (state) => {
      return state.images.filter((obj) => obj.detection_type == "images");
    },
    getHandwritingImages: (state) => {
      return state.images.filter((obj) => obj.detection_type == "handwriting");
    },
    getColorAuthor: (state) => {
      return state.colorAuthor;
    },
    getColorTitle: (state) => {
      return state.colorTitle;
    },
    getColorContent: (state) => {
      return state.colorContent;
    },
    getTerm: (state) => {
      return state.term;
    },
    getPageOptions: (state) => {
      return state.pageOptions;
    },
    getUsedSwitches: (state) => {
      return state.usedSwitches;
    },
    getActiveTableIndex: (state) => {
      return state.activeTableIndex;
    },
  },
  mutations: {
    setQuery(state, query) {
      state.query = query;
    },
    setColorAuthor(state, value) {
      state.colorAuthor = value;
    },
    setColorTitle(state, value) {
      state.colorTitle = value;
    },
    setColorContent(state, value) {
      state.colorContent = value;
    },
    setShowTable(state, showTable) {
      state.showTable = showTable;
    },
    setImages(state, payload) {
      const images = payload.results;
      const totalCount = payload.count;
      state.images = images;
      state.totalCount = totalCount;
      state.loading = false;
    },
    setTableSearchConfig(state, data) {
      state.tableTitles = data.tableTitles;
      state.usedSwitches = data.usedSwitches;
      state.term = data.term;
      state.activeTableIndex = data.activeTableIndex;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
    setAciveTableIndex(state, activeIndex) {
      state.activeTableIndex = activeIndex;
    },
    setPageOptions(state, pageOptions) {
      state.pageOptions = pageOptions;
    },
  },
  actions: {
    async fetchImages({ commit, getters }) {
      commit("setLoading", true);
      const term = getters.getTerm;
      const usedSwitches = getters.getUsedSwitches;
      const pageOpitons = getters.getPageOptions;

      let params = new URLSearchParams();
      params.append("term", term);
      params.append("page_size", pageOpitons.itemsPerPage.toString());
      params.append("page", pageOpitons.page.toString());
      for (var key in usedSwitches) {
        params.append(key, usedSwitches[key]);
      }
      params.append("detection_types", getters.getActiveTableIndex);
      await axios
        .get("/api/v1/images/", {
          params: params,
        })
        .then((response) => {
          commit("setImages", response.data);
        });
    },
  },
  modules: {},
});
