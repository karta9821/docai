import Vue from "vue";
import VueRouter from "vue-router";
import Search from "../views/Search.vue";
import Upload from "../views/Upload.vue";

Vue.use(VueRouter);
const routes = [
  { path: '/', 
    redirect: { name: 'Search' } 
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
  },
  {
    path: "/upload",
    name: "Upload",
    component: Upload,
  },
  {
    path :'*',
    redirect: { name: 'Search' } 
}
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

const DEFAULT_TITLE = "DocAI";
router.afterEach((to) => {
  Vue.nextTick(() => {
    document.title = `${to.name} - ${DEFAULT_TITLE}` || DEFAULT_TITLE;
  });
});

export default router;
