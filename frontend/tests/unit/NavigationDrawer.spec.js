import { shallowMount } from "@vue/test-utils";
import NavigationDrawer from "@/components/NavigationDrawer.vue";
import Vuetify from "vuetify";

const vuetify = new Vuetify();

describe("NavigationDrawer.vue", () => {
  it("should be named 'DocAI'", () => {
    const text = "DocAI";
    const wrapper = shallowMount(NavigationDrawer, {
      mocks: {
        $vuetify: { breakpoint: {} },
      },
      vuetify,
    });
    console.log(wrapper.text);
    expect(wrapper.text()).toMatch(text);
  });
});
