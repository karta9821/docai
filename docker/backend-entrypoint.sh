#!/bin/sh

echo "Waiting for postgres..."

until nc -z $SQL_HOST $SQL_PORT; do
  sleep 1
done

echo "PostgreSQL started"

python3 manage.py makemigrations
python3 manage.py migrate

exec "$@"
