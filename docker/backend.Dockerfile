FROM python:3.8
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get -y install netcat poppler-utils ffmpeg libsm6 libxext6 tesseract-ocr \
    libtesseract-dev libleptonica-dev pkg-config tesseract-ocr-pol
WORKDIR /opt/backend/

COPY requirements.txt .
COPY dev-requirements.txt .

RUN pip install -r requirements.txt -r dev-requirements.txt 
RUN pip install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html