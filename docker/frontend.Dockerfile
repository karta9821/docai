FROM node:15.13-alpine

WORKDIR /opt/frontend/

COPY frontend/package*.json ./

RUN npm install
RUN npm install @vue/cli -g