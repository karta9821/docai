COMPOSE_FILE_PATH := -f docker-compose.yml
COMPOSE_PROD_FILE_PATH := -f docker-compose.prod.yml
help:           ## Show this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

build: ## Build developer docker images
	docker-compose build

up: ## Run developer docker images
	docker-compose $(COMPOSE_FILE_PATH) up -d

django-shell: ## Run django shell in developer container
	docker-compose $(COMPOSE_FILE_PATH) run backend python manage.py shell

stop: ## Stop developer docker images
	@docker-compose stop

restart:
	@make -s stop
	@make -s up

install: ## Install requrirements packages
	pip install pip-tools
	pip install -r requirements.txt -r dev-requirements.txt
	(cd frontend && npm install)

lint: ## Run linters
	pre-commit run -a

test: ## Run tests
	docker-compose $(COMPOSE_FILE_PATH) run backend python manage.py test apps --pattern="tests_*.py"

install-pre-commit: ## Install pre-commit
	pre-commit install

build-prod: ## Build production docker images
	docker-compose ${COMPOSE_PROD_FILE_PATH} build

up-prod: ## Run production docker images
	docker-compose $(COMPOSE_PROD_FILE_PATH) up -d

django-shell-prod: ## Run django shell in production container
	docker-compose $(COMPOSE_PROD_FILE_PATH) run backend python manage.py shell