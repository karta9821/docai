from apps.base.models import Document
from django.db import models

# Create your models here.
from .consts import DetectionType
from .managers import ImageManager


class Image(models.Model):
    document = models.ForeignKey(
        Document, on_delete=models.CASCADE, related_name="images"
    )
    text = models.CharField(max_length=256, blank=True)
    image = models.ImageField(upload_to="images/")
    detection_type = models.CharField(
        max_length=11, choices=DetectionType.TYPES_CHOICES
    )

    objects = ImageManager()
