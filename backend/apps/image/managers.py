from django.db import models
from django.db.models import Q


class ImageManager(models.Manager):
    def search(self, types, term, detection_types):
        query = Q()
        query.add(Q(text__icontains=term), Q.OR)
        if types.get("title"):
            query.add(Q(document__title__icontains=term), Q.OR)
        if types.get("author"):
            query.add(Q(document__author__icontains=term), Q.OR)
        if types.get("description"):
            query.add(Q(document__description__icontains=term), Q.OR)
        return self.filter(detection_type__in=detection_types).filter(query)
