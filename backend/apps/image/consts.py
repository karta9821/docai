class DetectionType:
    IMAGE = "images"
    TABLE = "tables"
    HANDWRITING = "handwriting"

    TYPES = [IMAGE, TABLE, HANDWRITING]

    TYPES_CHOICES = ((IMAGE, "Image"), (TABLE, "Table"), (HANDWRITING, "Text"))
