from django.urls import path

from .api import ImageSearchAPIView

urlpatterns = [
    path("", ImageSearchAPIView.as_view(), name="images"),
]
