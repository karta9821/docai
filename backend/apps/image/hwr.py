import os

from apps.image.consts import DetectionType
from django.conf import settings
from apps.image.utils import detect, ocr_image, create_images


handwriting_detection_folder = f"{settings.MEDIA_ROOT}/images/handwriting_detection"


def get_results(base_path):
    results = []
    for image in os.listdir(f"{base_path}/crops/text line/"):
        ocr_text = ocr_image(f"{base_path}/crops/text line/{image}")[:256]
        abs_image_path = f"{base_path}/crops/text line/{image}"
        results.append((ocr_text, abs_image_path))
    return results

def run_detect_handwriting(instance_id, image_path, target_path):
    base_path = f"{handwriting_detection_folder}/{target_path}"
    detect(
        image_path,
        "best_hwr.pt",
        base_path,
        [0],
        0.5
    )
    if not os.path.exists(f"{base_path}/crops/text line/"):
        return
    results = get_results(base_path)
    create_images(instance_id, results, target_path, "hwr", DetectionType.HANDWRITING)
