import os

from apps.image.consts import DetectionType
from django.conf import settings
from apps.image.utils import detect, ocr_image, create_images

tables_detection_folder = f"{settings.MEDIA_ROOT}/images/table_detection"

def get_results(base_path):
    results = []
    for image in os.listdir(f"{base_path}/crops/Tables/"):
        ocr_text = ocr_image(f"{base_path}/crops/Tables/{image}")[:256]
        abs_image_path = f"{base_path}/crops/Tables/{image}"
        results.append((ocr_text, abs_image_path))
    return results


def run_detect_tables(instance_id, image_path, target_path):
    base_path = f"{tables_detection_folder}/{target_path}"
    detect(
        image_path,
        "best_tables.pt",
        base_path,
        [0],
        0.5
    )
    if not os.path.exists(f"{base_path}/crops/Tables/"):
        return
    results = get_results(base_path)
    create_images(instance_id, results, target_path, "table", DetectionType.TABLE)