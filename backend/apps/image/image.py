import os
from itertools import product

from apps.image.consts import DetectionType
from django.conf import settings

from apps.image.utils import detect, ocr_image, create_images

images_detection_folder = f"{settings.MEDIA_ROOT}/images/img_detection"


def run_detect_poi(image_path, target_path):
    project = f"{images_detection_folder}/{target_path}"
    detect(image_path, "best_images_poi.pt", project, [2], 0.2)



def get_ocr_results(base_path):
    if not os.path.exists(f"{base_path}/crops/Text"):
        return [""]
    return [
            ocr_image(f"{base_path}/crops/Text/{image}")[:256]
            for image in os.listdir(f"{base_path}/crops/Text/")
        ] or [""]

def get_image_results(base_path):
    return [
            f"{base_path}/crops/Image/{image}"
            for image in os.listdir(f"{base_path}/crops/Image")
        ]

def create_paths(crop, target_path):
    folder_name = os.path.basename(crop).split(".")[0]
    base_path = f"{images_detection_folder}/{target_path}/{folder_name}"
    image_path = f"{images_detection_folder}/{target_path}/crops/POI/{crop}"
    return base_path, image_path

def run_detect_images(instance_id, image_path, target_path):
    run_detect_poi(image_path, target_path)
    crops_path = f"{images_detection_folder}/{target_path}/crops"

    if not os.path.exists(crops_path):
        return

    for crop in os.listdir(f"{crops_path}/POI"):
        base_path, image_path = create_paths(crop, target_path)
        detect(image_path, "best_poi.pt", base_path, [0,1], 0.5)
        if not os.path.exists(f"{base_path}/crops/Image"):
            return
        ocr_results = get_ocr_results(base_path)
        image_results = get_image_results(base_path)
        results = product(ocr_results, image_results)
        create_images(instance_id, results, target_path, "img", DetectionType.IMAGE)