from unittest import mock
from django.test import TestCase
from apps.image.table import get_results, run_detect_tables

from apps.image.consts import DetectionType

from django.conf import settings


tables_detection_folder = f"{settings.MEDIA_ROOT}/images/table_detection"

class TableTestCase(TestCase):
    
    @mock.patch("apps.image.table.ocr_image")
    @mock.patch("apps.image.table.os.listdir")
    def test_get_results(self, mock_listdir, mock_ocr_image):
        base = "base"
        mock_listdir.return_value = ("some-path",)
        mock_ocr_image.return_value = "mock_ocr_value"
        
        results = get_results(base)

        mock_listdir.assert_called_once_with(f"{base}/crops/Tables/")
        mock_ocr_image.assert_called_once_with(f"{base}/crops/Tables/some-path")

        self.assertEqual(
            [("mock_ocr_value", f"{base}/crops/Tables/some-path")], results
        )
    
    @mock.patch("apps.image.table.os.path.exists", return_value=1)
    @mock.patch("apps.image.table.create_images")
    @mock.patch("apps.image.table.get_results")
    @mock.patch("apps.image.table.detect")
    def test_run_detect_tables(self, mock_detect, mock_get_results, mock_create_images, _):
        instance_id, image_path, target_path = ("1", "path", "target")

        run_detect_tables(instance_id, image_path, target_path)

        mock_detect.assert_called_once_with(
            image_path,
            "best_tables.pt",
            f"{tables_detection_folder}/{target_path}",
            [0],
            0.5
        )
        mock_get_results.assert_called_once_with(f"{tables_detection_folder}/{target_path}")
        mock_create_images.assert_called_once_with(instance_id, mock_get_results.return_value, target_path, "table", DetectionType.TABLE)
