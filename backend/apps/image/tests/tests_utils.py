from unittest import mock
from django.test import TestCase
from apps.image.utils import create_images, ocr_image,render_filename,create_image


class UtilsTestCase(TestCase):

    @mock.patch("apps.image.utils.file_to_text")
    def test_ocr_image(self, mock_file_to_text):
        path = "some-path"

        result = ocr_image(path)

        mock_file_to_text.assert_called_once_with(path, "pol")
        self.assertEqual(result, mock_file_to_text.return_value)

    @mock.patch("apps.image.utils.time.time", return_value=10)
    def test_render_filename(self, mock_time):
        base = "base"
        attribute = "attribute"

        result = render_filename(base, attribute)

        self.assertEqual(result, "base-attribute-10")
    
    @mock.patch("apps.image.utils.render_filename")
    @mock.patch("apps.image.utils.open")
    @mock.patch("apps.image.utils.UploadedFile")
    @mock.patch("apps.image.utils.apps")
    def test_create_image(self, mock_apps, mock_uploaded_file, mock_open, mock_render_filename):
        text =         "text"
        image  =         "image "
        target_path =         "target_path"
        document =         "document"
        name =         "name"
        detection_type =         "detection_type"

        create_image("", text, image, target_path, document, name, detection_type)

        mock_apps.get_model.assert_called_once_with("image", "Image")
        mock_open.assert_called_once_with(image, "rb")
        mock_render_filename.assert_called_once_with(target_path, name)
        mock_uploaded_file.assert_called_once_with(
            file=mock_open.return_value,
            name=mock_render_filename.return_value
        )
        mock_apps.get_model.return_value.objects.create.assert_called_once_with(
            document=document,
            text=text,
            image=mock_uploaded_file.return_value,
            detection_type=detection_type
        )
    @mock.patch("apps.image.utils.enumerate")
    @mock.patch("apps.image.utils.create_image")
    @mock.patch("apps.image.utils.apps")
    def test_create_images(self, mock_apps, mock_create_image, mock_enumerate):
        mock_enumerate.side_effect = mock.MagicMock(return_value=[(1, (1,2)),])
        instance_id, results, target_path, type_,detection_type = ("1", "results", "target_path", "type_","detection_type")

        create_images(instance_id, results, target_path, type_,detection_type)

        mock_apps.get_model.assert_called_once_with("base", "Document")
        mock_apps.get_model.return_value.objects.get.assert_called_once_with(id=instance_id)
        mock_create_image.assert_called_once()