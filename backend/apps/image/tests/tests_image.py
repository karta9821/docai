from unittest import mock
from django.test import TestCase
from apps.image.image import run_detect_poi,get_ocr_results, get_image_results, create_paths, run_detect_images

from apps.image.consts import DetectionType

from django.conf import settings


images_detection_folder = f"{settings.MEDIA_ROOT}/images/img_detection"

class ImageTestCase(TestCase):
    
    @mock.patch("apps.image.image.detect")
    def test_run_detect_poi(self, mock_detect):
        image_path, target_path = ("path1", "path2")

        run_detect_poi(image_path, target_path)

        mock_detect.assert_called_once_with(
            image_path,
            "best_images_poi.pt", f"{images_detection_folder}/{target_path}", [2], 0.2
        )
    
    @mock.patch("apps.image.image.os.path.exists", return_value=False)
    def test_get_ocr_results_when_no_exists(self, _):
        self.assertEqual(
            get_ocr_results(""), [""]
        )

    @mock.patch("apps.image.image.os.listdir", return_value=[])
    @mock.patch("apps.image.image.os.path.exists", return_value=True)
    def test_get_ocr_results_when_no_in_list_dir(self,_, __):
        self.assertEqual(
            get_ocr_results(""), [""]
        )
    @mock.patch("apps.image.image.ocr_image")
    @mock.patch("apps.image.image.os.listdir", return_value=["1"])
    @mock.patch("apps.image.image.os.path.exists", return_value=True)
    def test_get_ocr_result(self,_, __, mock_ocr_image):
        mock_ocr_image.return_value = "some text"
        self.assertEqual(
            get_ocr_results(""), ["some text"]
        )

    @mock.patch("apps.image.image.os.listdir", return_value=["1"])
    def test_get_image_results(self, mock_listdir):
        self.assertEqual(
            get_image_results("base"),
            ["base/crops/Image/1"]
        )

    @mock.patch("apps.image.image.os.listdir", return_value=[])
    def test_get_image_results_when_empoty(self, mock_listdir):
        self.assertEqual(
            get_image_results("base"),
            []
        )
    
    @mock.patch("apps.image.image.os.path.basename", return_value="some.path")
    def test_create_paths(self, _):
        self.assertEqual(
            create_paths("crop", "target"),
            ('/opt/backend/media/images/img_detection/target/some',
            '/opt/backend/media/images/img_detection/target/crops/POI/crop')
        )

    @mock.patch("apps.image.image.os.path.exists", return_value=False)
    @mock.patch("apps.image.image.run_detect_poi")
    def test_run_detect_images_when_path_does_not_exist(self, mock_poi, mock_exists):
        result = run_detect_images("1", "image", "target")

        mock_poi.assert_called_once()
        mock_exists.assert_called_once()
        self.assertIsNone(result)

    @mock.patch("apps.image.image.os.listdir", return_value=["1"])
    @mock.patch("apps.image.image.create_images")
    @mock.patch("apps.image.image.product")
    @mock.patch("apps.image.image.get_image_results")
    @mock.patch("apps.image.image.get_ocr_results")
    @mock.patch("apps.image.image.detect")
    @mock.patch("apps.image.image.os.path.exists", return_value=True)
    @mock.patch("apps.image.image.run_detect_poi")
    def test_run_detect_images_when_path(self, mock_poi, mock_exists, mock_detect, mock_get_ocr_results, mock_get_image_results, mock_product, mock_create_images, mock_listdir):
        result = run_detect_images("1", "image", "target")

        mock_poi.assert_called_once()
        mock_listdir.assert_called_once()
        mock_get_ocr_results.assert_called_once()
        mock_detect.assert_called_once()
        mock_get_image_results.assert_called_once()
        mock_product.assert_called_once()
        mock_create_images.assert_called_once()

        self.assertIsNone(result)
