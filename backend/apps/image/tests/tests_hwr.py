from unittest import mock
from django.test import TestCase
from apps.image.hwr import get_results, run_detect_handwriting

from apps.image.consts import DetectionType

from django.conf import settings


handwriting_detection_folder = f"{settings.MEDIA_ROOT}/images/handwriting_detection"

class HWRTestCase(TestCase):
    
    @mock.patch("apps.image.hwr.ocr_image")
    @mock.patch("apps.image.hwr.os.listdir")
    def test_get_results(self, mock_listdir, mock_ocr_image):
        base = "base"
        mock_listdir.return_value = ("some-path",)
        mock_ocr_image.return_value = "mock_ocr_value"
        
        results = get_results(base)

        mock_listdir.assert_called_once_with(f"{base}/crops/text line/")
        mock_ocr_image.assert_called_once_with(f"{base}/crops/text line/some-path")

        self.assertEqual(
            [("mock_ocr_value", f"{base}/crops/text line/some-path")], results
        )
    
    @mock.patch("apps.image.hwr.os.path.exists", return_value=1)
    @mock.patch("apps.image.hwr.create_images")
    @mock.patch("apps.image.hwr.get_results")
    @mock.patch("apps.image.hwr.detect")
    def test_run_detect_handwriting(self, mock_detect, mock_get_results, mock_create_images, _):
        instance_id, image_path, target_path = ("1", "path", "target")

        run_detect_handwriting(instance_id, image_path, target_path)

        mock_detect.assert_called_once_with(
            image_path,
            "best_hwr.pt",
            f"{handwriting_detection_folder}/{target_path}",
            [0],
            0.5
        )
        mock_get_results.assert_called_once_with(f"{handwriting_detection_folder}/{target_path}")
        mock_create_images.assert_called_once_with(instance_id, mock_get_results.return_value, target_path, "hwr", DetectionType.HANDWRITING)
