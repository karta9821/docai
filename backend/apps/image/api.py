from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import exceptions
from rest_framework.generics import ListAPIView

from .consts import DetectionType
from .models import Image
from .pagination import CustomLimitOffsetPagination
from .serializers import ImageSerializer


class ImageSearchAPIView(ListAPIView):
    pagination_class = CustomLimitOffsetPagination
    serializer_class = ImageSerializer

    def get_queryset(self):
        term = self.request.query_params.get("term")
        detection_types = self.request.query_params.getlist("detection_types", "")
        if not term:
            raise exceptions.APIException("term parameter is required")
        try:
            return Image.objects.search(
                self.request.query_params, term, detection_types
            )
        except Exception as e:
            raise exceptions.APIException(str(e))

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name="term",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                required=True,
            ),
            openapi.Parameter(
                name="type",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description="allowed types are:\n title, author, content",
                required=True,
            ),
            openapi.Parameter(
                name="detection_types",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_ARRAY,
                items=openapi.Items(openapi.TYPE_STRING),
                description="allowed types are:\n" + ", ".join(DetectionType.TYPES),
                required=True,
            ),
        ]
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
