from apps.base.serializers import DocumentSerializer
from rest_framework import serializers

from .models import Image


class ImageSerializer(serializers.ModelSerializer):
    document = DocumentSerializer()

    class Meta:
        model = Image
        fields = ["text", "image", "detection_type", "document"]
