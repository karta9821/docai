from typing import List
import time
from yolov5 import detect as yolov5_detect
from django.apps import apps
from django.core.files.uploadedfile import UploadedFile

from tesserocr import file_to_text

def ocr_image(img_path):
    return file_to_text(img_path, 'pol')

def render_filename(base: str, attribute: str) -> str:
    return f"{base}-{attribute}-{int(time.time())}"

def detect(image_path: str, weights: str, project: str, classes: List[int], conf_thres: float) -> None:
    yolov5_detect.run(
        source=image_path,
        weights=weights,
        project=project,
        name=f"",
        classes=classes,
        nosave=True,
        save_crop=True,
        conf_thres=conf_thres,
    )

def create_image(idx, text, image, target_path, document, name, detection_type):
    Image = apps.get_model("image", "Image")
    Image.objects.create(
                document=document,
                text=text,
                image=UploadedFile(
                    file=open(image, "rb"),
                    name=render_filename(target_path, name),
                ),
                detection_type=detection_type,
            )

def create_images(instance_id, results, target_path, type_,detection_type):
    Document = apps.get_model("base", "Document")
    document = Document.objects.get(id=instance_id)
    for idx, (text, image) in enumerate(results):
        create_image(idx, text, image, target_path, document, f"{type_}{idx}", detection_type)