from apps.base.consts import DocumentStatusConst
from django.apps import apps
from docai.celery import app
from apps.image.image import run_detect_images
from apps.image.hwr import run_detect_handwriting
from apps.image.table import run_detect_tables

@app.task()
def run_detect(instance_id, image_path, target_path):
    run_detect_images(instance_id, image_path, target_path)
    run_detect_tables(instance_id, image_path, target_path)
    run_detect_handwriting(instance_id, image_path, target_path)

    Document = apps.get_model("base", "Document")
    document = Document.objects.get(id=instance_id)
    document.status = DocumentStatusConst.DONE
    document.save()
