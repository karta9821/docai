from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from .api import DocumentAPIView, DocumentGetRecentAPIView

schema_view = get_schema_view(
    openapi.Info(
        title="DocAI API",
        default_version="v1",
        description="DocAI API",
    ),
    public=True,
    authentication_classes=(),
    permission_classes=(),
)

urlpatterns = [
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("upload/", DocumentAPIView.as_view(), name="document-upload"),
    path("get-recent/", DocumentGetRecentAPIView.as_view(), name="document-search"),
    path("images/", include("apps.image.urls")),
]
