from django.db import models
from django.db.models import fields
from rest_framework import serializers

from .models import Document


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = [
            "author",
            "title",
            "description",
            "file",
            "created_at",
            "id",
            "status",
        ]
