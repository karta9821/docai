class DocumentStatusConst:
    PROCESSING = "PROCESSING"
    DONE = "DONE"

    STATUSES = ((PROCESSING, "Processing"), (DONE, "Done"))
