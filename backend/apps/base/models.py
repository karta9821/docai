import os

from apps.base.consts import DocumentStatusConst
from apps.base.managers import DocumentManager
from apps.image.tasks import run_detect
from django.core.validators import FileExtensionValidator
from django.db import models
from django.db.models.signals import post_save


class Document(models.Model):
    author = models.CharField(max_length=150, blank=False)
    title = models.CharField(max_length=150, blank=False)
    description = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    file = models.FileField(
        upload_to="documents/",
        validators=[FileExtensionValidator(allowed_extensions=["jpg"])],
    )
    status = models.CharField(
        max_length=15,
        choices=DocumentStatusConst.STATUSES,
        default=DocumentStatusConst.PROCESSING,
    )

    objects = DocumentManager()

    @property
    def filename(self):
        return os.path.basename(self.file.name)

    @property
    def no_extension_filename(self):
        return self.filename.split(".")[0]


def document_post_save(sender, instance, created, *args, **kwargs):
    if not created:
        return
    run_detect.apply_async(
        (instance.id, instance.file.path, instance.no_extension_filename)
    )


post_save.connect(document_post_save, sender=Document)
