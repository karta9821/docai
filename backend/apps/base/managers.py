from apps.image.consts import DetectionType
from django.db import models


class DocumentManager(models.Manager):
    def search(self, type, term):
        if type in ["title", "author", "description"]:
            return self.filter(**{f"{type}__icontains": term})
        elif type in DetectionType.TYPES:
            return self.filter(
                images__detection_type=type, images__text__icontains=term
            )
        else:
            raise Exception(f"Search for type {type} not allowed")
