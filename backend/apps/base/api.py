from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.parsers import MultiPartParser

from .models import Document
from .pagination import CustomLimitOffsetPagination
from .serializers import DocumentSerializer


class DocumentAPIView(CreateAPIView):
    parser_classes = [MultiPartParser]
    serializer_class = DocumentSerializer


class DocumentGetRecentAPIView(ListAPIView):
    pagination_class = CustomLimitOffsetPagination
    serializer_class = DocumentSerializer

    def get_queryset(self):
        return Document.objects.order_by("-created_at")[:5]

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
