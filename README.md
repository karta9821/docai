# DocAI

More information about project is stored in docs/ folder.

## Frontend URL
```
http://localhost:8080/
```
## Backend URL
```
http://localhost:8000/
```
## Run in Docker

You need to install Docker and Docker-compose

### Running in docker
To build:

```
make build
```

To run:
```
make up
```

## Locally

### Requirements

* Python 3.10
* PostgreSQL
* Node v15.13.0

Create virtual environment
```sh
python3 -m venv venv
. venv/bin/activate
```

Install requirements:
```sh
make install
```

Create .env file in backend directory & copy variables:
```sh
SECRET_KEY=SECRET_KEY
DEBUG=1
ALLOWED_HOSTS=localhost 127.0.0.1
SQL_HOST=localhost
SQL_PORT=5432
POSTGRES_PASSWORD=docai
POSTGRES_DB=docai
POSTGRES_USER=docai
```

### Create database:
```sh
sudo -u postgres psql
```
```sql
CREATE DATABASE docai;
CREATE USER docai WITH PASSWORD 'docai';
GRANT ALL PRIVILEGES ON DATABASE docai TO docai;
```
Allow user to create databases:
```sql
ALTER USER docai CREATEDB;
```

## Run
```sh
(cd backend && python3 manage.py runserver)
(cd frontend && npm run serve)
```

## Install pre-commit
1. Run command in project directory
```
make install-pre-commit
```

## Run linters
1. Run command in project directory
```
make lint
```

