# Basic information

Repository with initial concept - https://git.wmi.amu.edu.pl/s470611/pbr-ocr-proj

## Backlog

All tasks and issues are documented in Jira (jira.wmi.amu.edu.pl)

Basic flow of the app is represented in miro (https://miro.com/app/board/uXjVOfRamCs=/?invite_link_id=46696601107)

## Gitlab

- Each MR should be approved by 2 maintainers.
- Branches should be named: **main** and **feature/DOCAI-***

## Dev server

URL: http://docai.projektstudencki.pl/

## Documentation

All documentation should be stored in this folder.
