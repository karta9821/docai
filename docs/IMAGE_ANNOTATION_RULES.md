## Annotowanie obrazków
#### nie annotujemy:
- ornamentów, szlaczków, 
- obrazków typu logo, i logo w tytułach
- symboli tj. strzałki, wskaźniki 
- tabel, krzyżówek (ale zadania typu "znajdź różnicę między 2 obrazkami" zaznaczamy)

#### preferujemy prostokąty (ale jeżeli potrzebujemy zaznaczyć coś nieforemnego, to polygony też mogą być)

#### wykresy, przekroje, mapy, wykroje to obrazki

#### podpisy
- podpisy tekstowe zaznaczamy tylko wtedy, kiedy opis może naprowadzać na to, co się na obrazku znajduje
- podpis nie musi być pod, moze być nad, obok lub na obrazku
- jeśli obraz zawiera więcej niż jeden podpis - zaznaczamy wszystkie
- podpisem jest również autor fotografii
- podpis obrazka będącego przedmiotem reklamy
- grupowanie przez zaznaczenie w jeden obszar obrazka i tekstu (label Image+Text)


## Annotowanie pisma ręcznego
#### typy labeli: 
- nagłówki - title
- tekst właściwy - text
- źródło - note

#### osobno zaznaczamy każdy wiersz tekstu

#### preferowany prostokąt; chyba że pismo jest pochylone - wtedy równoległobok (jako polygon)

#### prostokąty mogą na siebie nachodzić, ale nie przykrywać

#### niepełne wiersze zaznaczamy do końca tekstu, a nie do końca kartki